package com.amaca.changemycolor

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    val redColor by lazy { ContextCompat.getColor(this, R.color.red_1) }

    val blueColor by lazy { ContextCompat.getColor(this, R.color.blue_1) }

    val greenColor by lazy { ContextCompat.getColor(this, R.color.green_1) }

    val orangeColor by lazy {ContextCompat.getColor(this, R.color.orange_1)}


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val colorViewPanel = findViewById<View>(R.id.color_view_panel)

        findViewById<Button>(R.id.red_button).setOnClickListener {
            colorViewPanel.setBackgroundColor(redColor)
        }

        findViewById<Button>(R.id.blue_button).setOnClickListener {
            colorViewPanel.setBackgroundColor(blueColor)
        }

        findViewById<Button>(R.id.green_button).setOnClickListener {
            colorViewPanel.setBackgroundColor(greenColor)
        }

        findViewById<Button>(R.id.orange_button).setOnClickListener {
            colorViewPanel.setBackgroundColor(orangeColor)
        }
    }
}